module bitbucket.org/uwaploe/dvlgs4

go 1.13

require (
	bitbucket.org/uwaploe/go-dvl v0.9.6
	github.com/nats-io/stan.go v0.6.0
)
