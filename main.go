// Dvlgs4 reads the raw DVL data records from the NATS Streaming Server
// and sends them to a UDP port on the GS4.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/nats-io/stan.go"
)

const Usage = `Usage: dvlgs4 [options] host:port

Subscribe to the raw DVL data and forward the data records to the GS4
at HOST:PORT.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL    string = "nats://localhost:4222"
	clusterID  string = "must-cluster"
	dvlSubject string = "dvlraw.data"
	fixCoord   bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&dvlSubject, "sub", lookupEnvOrString("DVL_RAW_SUBJECT", dvlSubject),
		"Subject name for raw DVL data")
	flag.BoolVar(&fixCoord, "fix", false, "Include fix for GS4 DVL coordinate mismatch")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

type delayMsg struct {
	T     time.Time `json:"t"`
	Delay int64     `json:"delay"`
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	gs4Addr := args[0]

	sc, err := stan.Connect(clusterID, "gs4-dvl", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	conn, err := net.Dial("udp", gs4Addr)
	if err != nil {
		log.Fatal("Cannot access GS4")
	}

	dataCb := func(m *stan.Msg) {
		d := delayMsg{T: time.Now().UTC()}
		d.Delay = d.T.UnixNano() - m.Timestamp
		if fixCoord {
			dvlCoordFix(m.Data)
		}
		_, err := conn.Write(m.Data)
		if err != nil {
			log.Printf("UDP write error: %v", err)
		}

		if b, err := json.Marshal(d); err == nil {
			sc.Publish("dvl.delay", b)
		}
	}

	sub, err := sc.Subscribe(dvlSubject, dataCb, stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to raw DVL data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("GS4 DVL service starting %s", Version)

	// Exit on a signal
	<-sigs
}
