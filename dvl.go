package main

import "bitbucket.org/uwaploe/go-dvl"

// Work-around a coordinate system error on the GS4 by swapping the X and Y velocities
// in the DVL data record and flipping the sign of the Z velocity.
func dvlCoordFix(rec []byte) {
	var vx, vy int16

	word := dvl.ByteOrder.Uint16(rec[5:])
	vx = int16(word)

	word = dvl.ByteOrder.Uint16(rec[7:])
	vy = int16(word)

	word = uint16(vy)
	dvl.ByteOrder.PutUint16(rec[5:], word)
	word = uint16(vx)
	dvl.ByteOrder.PutUint16(rec[7:], word)

}
