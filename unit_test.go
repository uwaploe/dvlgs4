package main

import (
	"bytes"
	"encoding/binary"
	"testing"

	"bitbucket.org/uwaploe/go-dvl"
)

func TestCoordFix(t *testing.T) {
	rec := dvl.PD4{}
	rec.VelBtm = [4]int16{1000, -2000, 500}

	expected := [4]int16{-2000, 1000, 500}

	var b bytes.Buffer
	err := binary.Write(&b, dvl.ByteOrder, rec)
	if err != nil {
		t.Fatal(err)
	}
	dvlCoordFix(b.Bytes())

	binary.Read(&b, dvl.ByteOrder, &rec)
	if rec.VelBtm != expected {
		t.Errorf("Bad value; expected %v, got %v", expected, rec.VelBtm)
	}
}
